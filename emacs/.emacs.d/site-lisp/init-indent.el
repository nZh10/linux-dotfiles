(require 'smart-tabs-mode)
(autoload 'smart-tabs-mode "smart-tabs-mode"
  "Intelligently indent with tabs, align with spaces!")
(autoload 'smart-tabs-mode-enable "smart-tabs-mode")
(autoload 'smart-tabs-advice "smart-tabs-mode")
(autoload 'smart-tabs-insinuate "smart-tabs-mode")

(smart-tabs-insinuate 'c 'c++ 'java 'javascript 'cperl 'python 'ruby)

(setq-default tab-width 4)
(defvaralias 'c-basic-offset 'tab-width)
(defvaralias 'cperl-indent-level 'tab-width)
(setq-default c-default-style '((c-mode . "bsd")
								(c++-mode . "bsd")
								(java-mode . "java")
								(awk-mode . "awk")
								(other . "gnu")))
(defun shiguresuki/c-mode-hook ()
  (setq c-indent-level 4
		insert-tabs-mode t))
(add-hook 'c-mode-common-hook 'shiguresuki/c-mode-hook)

(provide 'init-indent)
