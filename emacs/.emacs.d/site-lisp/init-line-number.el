(dolist (hook (list
               'c-mode-common-hook
               'c-mode-hook
               'emacs-lisp-mode-hook
               'lisp-interaction-mode-hook
               'lisp-mode-hook
               'java-mode-hook
               'asm-mode-hook
               'haskell-mode-hook
               'sh-mode-hook
               'makefile-gmake-mode-hook
			   'perl-mode-hook
               'python-mode-hook
               'js-mode-hook
               'go-mode-hook
               'markdown-mode-hook
               'package-menu-mode-hook
               'cmake-mode-hook
               'rust-mode-hook
               'ruby-mode-hook
               'lua-mode-hook
               ))
  (add-hook hook (lambda () (display-line-numbers-mode))))
(setq-default display-line-numbers-type 'relative)
(provide 'init-line-number)
;;; init-line-number.el ends here
