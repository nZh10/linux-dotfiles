(require 'flymake-cursor)
(add-hook 'prog-mode-hook 'flymake-cursor-mode-hook)
(provide 'init-flymake-cursor)
