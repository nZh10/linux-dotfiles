;; .emacs --- My emacs config
;;; Commentary:
;;; Code:

;; Move to top to fix package-selected-package
;; see https://github.com/jwiegley/use-package/issues/397
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

;; load package manager, and add the THU mirror package registry
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives '(("gnu"   . "https://elpa.emacs-china.org/gnu/")
                         ("melpa" . "https://elpa.emacs-china.org/melpa/")))
(package-initialize)

;; bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))

(use-package evil
  :ensure t ;; install `evil' if not installed
  :init ;; tweak configurations of `evil' before loading it
  (setq evil-search-module 'evil-search)
  (setq evil-ex-complete-emacs-commands nil)
  (setq evil-vsplit-window-right t)
  (setq evil-split-window-below t)
  (setq evil-shift-round nil)
  (setq evil-want-C-u-scroll t)
  :config ;; tweak evil after loading it
  (evil-mode))

(use-package lsp-mode :commands lsp)
(use-package lsp-ui :commands lsp-ui-mode)
(use-package company-lsp :commands company-lsp)

;(use-package ccls :hook ((c-mode c++-mode objc-mode) .
;					(lambda () (require 'ccls) (lsp))))

(setq tab-width 4)
(setq scala-indent:step 4)
(defvaralias 'c-basic-offset 'tab-width)
(defvaralias 'cperl-indent-level 'tab-width)
(setq-default c-default-style "bsd")
(setq display-line-numbers-type 'relative)
(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(electric-pair-mode 1)
(setq my-acm-directory-list (list "~/src/c_cpp/ACM"
				  "~/src/java/ACM"))

(delete-selection-mode 1)
(provide 'init)
;;; init.el ends here
