#!/bin/bash
if [ "$(id -u)" == "0" ]; then
		echo "This script cannot be run as root." 1>&2
		exit 1
fi
sys=
if [ -f /usr/bin/zypper ]; then
    sys=1 # openSUSE
elif [ -f /usr/bin/apt ]; then
    sys=2 # debian-like (ubuntu)
elif [ -f /usr/bin/pacman ]; then
    sys=3 # arch-like (manjaro)
else
    echo "This system is not supported." 1>&2
    return 1
fi
function install_program() {
    if [ $sys == 1 ]; then
        sudo zypper install $1
    elif [ $sys == 2 ]; then
				sudo apt install $1
    elif [ $sys == 3 ]; then
				sudo pacman -S $1
    fi
}

[[ ! -f /usr/bin/stow ]] && install_program stow
[[ ! -f /usr/bin/zsh ]] && install_program zsh

# emacs
echo -n "Do you need emacs configuration? (Y/n) "
read input1
if [ $input1 == 'Y' ] || [ $input1 == 'y' ] || [ $input1 == 'yes' ]
then
    [[ ! -f /usr/bin/emacs ]] && install_program emacs
    stow emacs -t ~
fi

mkdir -pv .config
# git, you must install it before running the script ( or you cannot download it ;) )
stow git -t ~/.config

# nano
stow nano -t ~/.config

# screen, it seems not to support the xdg config.
echo -n "Do you need GNU Screen configuration? (Y/n) "
read input1
if [ $input1 == 'Y' ] || [ $input1 == 'y' ] || [ $input1 == 'yes' ]
then
    [[ ! -f /usr/bin/screen ]] && install_program screen
    stow screen -t ~
fi

# the core, shell
echo ". $HOME/.local/share/dotfiles/shell/init.sh" >> ~/.bashrc
echo ". $HOME/.local/share/dotfiles/shell/init.sh" >> ~/.zshrc
mkdir -pv ~/.cache/zsh

# vim
echo "source ~/.local/share/dotfiles/vim-init/init.vim" >> ~/.vimrc

# z-lua, so check if lua has installed
echo -n "Do you need z-lua (which needs to install lua)? (Y/n) "
read input1
if [ $input1 == 'Y' ] || [ $input1 == 'y' ] || [ $input1 == 'yes' ]
then
    [[ ! -f /usr/bin/lua ]] && install_program lua
fi

echo "Finish."
unset sys
unset input1
